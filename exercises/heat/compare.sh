#!/bin/bash

function getIter () {
  inFile=$1
  cat ${inFile} | grep -i "calc iter" | tail -n 1 | awk '{print $4}'
}

dashIter=$(getIter ../dash/log.txt)
openMPIIter=$(getIter ../openmpi/log.txt)
speedup=$(echo "scale=1; ${dashIter} / ${openMPIIter}" | bc)
echo "OpenMPI is ~${speedup} faster than DASH"
