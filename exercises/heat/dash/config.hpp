#include <libdash.h>

#define NUM_DIM 2
#define MASTER_ID 0

using index_t = long int;
using val_t = double;
using Matrix = dash::NArray<val_t, NUM_DIM>;
using Pattern = dash::BlockPattern<NUM_DIM, (dash::MemArrange)1, index_t>;
using LocalMatrix = Matrix::local_type;
using Coordinates = std::array<index_t, NUM_DIM>;
struct HaloBorder {
  std::vector<val_t> items;
  dash::Future<val_t*> future;
  const index_t globalIndex;
};
struct SimulationParams {
  const double dX, dY, dT, k, d2X, d2Y;
};
struct Neighbourhood {
  val_t up, down, left, right, me;
};
