#include "config.hpp"

bool amIMaster() { return dash::myid() == MASTER_ID; }

void printIt(const std::vector<val_t>& v) {
  for (auto it = v.begin(); it != v.end(); ++it) {
    std::cout << *it << " ";
  }
}

void printIt(const Matrix* m) {
  for (auto i = 0; i < m->extent(0); ++i) {
    for (auto j = 0; j < m->extent(1); ++j) {
      val_t item = m->at(i, j);
      printf("%5.4f ", item);
    }
    printf("\n");  // new row -> new line
  }
}

int mod(index_t a, index_t b) {
  int x = a;
  while (x < 0) {
    x += b;
  }
  return x % b;
}

index_t nextInTorus(index_t i, index_t maxI) { return mod(i + 1, maxI); }

index_t prevInTorus(index_t i, index_t maxI) { return mod(i - 1, maxI); }

int getArgs(index_t argc, char* argv[], std::vector<int>* args) {
  if (argc != 4) {
    perror("args: <x-size> <y-size> <iterations>");
    return 1;
  }

  for (auto i = 0; i < 3; ++i) {                 // argv 0 is the name of the
    int parsed = strtol(argv[i + 1], NULL, 10);  // parse as int
    args->push_back(parsed);
  }

  return 0;
}

void printMinimonSummary(MiniMon& minimon) {
  for (auto i = 0; i < dash::Team::All().size(); ++i) {
    minimon.print(i);
  }
}
