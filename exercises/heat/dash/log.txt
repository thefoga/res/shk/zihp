make[1]: Entering directory '/home/stefano/scratch/zihp/exercises/heat/dash'
mpirun -np 2 ./out 4000 4000 100
Running Jacobi on 2 units.
Array Dimensions: 4000 4000
Block Dimensions: 2000 4000
== Energy Conservation Check ==
initial Energy:         8000000.0000
  final Energy:         8000000.0000
    Difference:              -0.0000
== Time Statistics ==
Completed 100 iterations.
 0              name        num      avg      min      max        %
          calc total          1 6.202049 6.202049 6.202049
               total          1 7.403257 7.403257 7.403257
         update halo        100 0.003819 0.003249 0.006843   6.16 %
          calc inner        100 0.053944 0.043405 0.083072  86.98 %
           wait halo        100 0.000000 0.000000 0.000001   0.00 %
           calc halo        100 0.000374 0.000244 0.001265   0.60 %
           calc iter        100 0.062019 0.047042 0.098558
 1              name        num      avg      min      max        %
          calc total          1 6.202049 6.202049 6.202049
               total          1 7.403257 7.403257 7.403257
         update halo        100 0.003819 0.003249 0.006843   6.16 %
          calc inner        100 0.053944 0.043405 0.083072  86.98 %
           wait halo        100 0.000000 0.000000 0.000001   0.00 %
           calc halo        100 0.000374 0.000244 0.001265   0.60 %
           calc iter        100 0.062019 0.047042 0.098558
make[1]: Leaving directory '/home/stefano/scratch/zihp/exercises/heat/dash'
