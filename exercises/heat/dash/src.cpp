#include <libdash.h>
#include <algorithm>

#include "../common/minimon.h"
#include "utils.hpp"

MiniMon minimon{};

constexpr val_t dX = 1.0, dY = 1.0;
constexpr val_t d2X = pow(dX, 2);
constexpr val_t d2Y = pow(dY, 2);
static const struct SimulationParams params = {dX, dY, 0.05, 1.0, d2X, d2Y};

void getHaloRow(const Matrix* m, struct HaloBorder& border) {
  auto tmp = m->row(border.globalIndex);
  border.future = dash::copy_async(tmp.begin(), tmp.end(), border.items.data());
}

void getLocalRow(const LocalMatrix& loc,
                 const index_t row,
                 std::vector<val_t>& target) {
  auto tmp = loc.row(row);
  dash::copy(tmp.begin(), tmp.end(), target.data());
}

void getHaloColumn(const Matrix* m, struct HaloBorder& border) {
  auto tmp = m->col(border.globalIndex);
  border.future = dash::copy_async(tmp.begin(), tmp.end(), border.items.data());
}

void getLocalColumn(const LocalMatrix& loc,
                    const index_t column,
                    std::vector<val_t>& target) {
  auto tmp = loc.col(column);
  dash::copy(tmp.begin(), tmp.end(), target.data());
}

index_t haloRowIndex(const Matrix* m, index_t localRow) {
  const index_t globalRow = m->pattern().global({localRow, 0})[0];

  if (localRow == 0) {
    return prevInTorus(globalRow, m->extent(0));
  }

  // last local
  return nextInTorus(globalRow, m->extent(0));
}

index_t haloColumnIndex(const Matrix* m, index_t localColumn) {
  const index_t globalColumn = m->pattern().global({0, localColumn})[1];

  if (localColumn == 0) {
    return prevInTorus(globalColumn, m->extent(1));
  }

  // last local
  return nextInTorus(globalColumn, m->extent(1));
}

struct Neighbourhood getNeighbourhood(const index_t row,
                                      const index_t column,
                                      LocalMatrix& loc) {
  return {loc.at(row - 1, column),
          loc.at(row + 1, column),
          loc.at(row, column - 1),
          loc.at(row, column + 1),
          loc.at(row, column)};
}

void setValue(Matrix* m, const val_t value) {
  LocalMatrix& loc = m->local;  // local matrix
  for (index_t i = 0; i < loc.extent(0); ++i) {
    for (index_t j = 0; j < loc.extent(1); ++j) {
      loc.at(i, j) = value;
    }
  }
}

void allocate(Matrix* m, const val_t value = 0.0) {
  setValue(m, value);
  m->barrier();  // wait until all have filled it
}

void init(Matrix* m, const val_t value = 1.0) {
  if (amIMaster()) {
    for (index_t i = 0; i < m->local.extent(0); ++i) {
      for (index_t j = 0; j < m->local.extent(1); ++j) {
        m->local.at(i, j) = value;
      }
    }
  }
  m->barrier();
}

val_t sum(const Matrix* m) {
  const LocalMatrix& loc = m->local;  // local matrix  // todo as arg
  return std::accumulate(loc.begin(), loc.end(), 0.0);
}

val_t distributedSum(const Matrix* m) {
  size_t nUnits = dash::Team::All().size();
  dash::Array<val_t> sums(nUnits,
                          dash::BLOCKED);  // distributed array containing sums

  sums.local[0] = sum(m);  // each unit sums their local data
  sums.barrier();          // wait until everybody has summed their part
  return std::accumulate(sums.begin(), sums.end(), 0.0);
}

val_t getIteration(const struct Neighbourhood neighbourhood) {
  const val_t me = neighbourhood.me;
  const val_t dThetaX =
      (neighbourhood.up + neighbourhood.down - 2 * me) / params.d2X;
  const val_t dThetaY =
      (neighbourhood.left + neighbourhood.right - 2 * me) / params.d2Y;
  const val_t dTheta = dThetaX + dThetaY;
  return me + params.k * dTheta * params.dT;
}

void doIteration(LocalMatrix& loc,
                 const index_t i,
                 const index_t j,
                 const struct Neighbourhood neighbourhood) {
  loc.at(i, j) = getIteration(neighbourhood);
}

void calcInnerGrid(LocalMatrix& loc, LocalMatrix& locNew) {
  constexpr index_t offset =
      1;  // # cells already computed by the halo rows and columns
  for (auto i = offset; i < loc.extent(0) - offset; ++i) {
    for (auto j = offset; j < loc.extent(1) - offset; ++j) {
      const struct Neighbourhood neighbourhood = getNeighbourhood(i, j, loc);
      doIteration(locNew, i, j, neighbourhood);
    }
  }
}

void calcHaloRows(const Matrix* m,
                  LocalMatrix& loc,
                  LocalMatrix& locNew,
                  const std::array<HaloBorder, 2>& rowBorders,
                  const std::array<HaloBorder, 2>& columnBorders) {
  constexpr index_t offset = 0;  // # cells already computed by the halo columns
  const Coordinates globalCoordinates = m->pattern().global({0, 0});
  const index_t globalRow = globalCoordinates[0];
  const val_t haloRight = columnBorders[0].items.at(globalRow);
  const val_t haloLeft = columnBorders[1].items.at(globalRow);

  // last row
  index_t rowIndex = loc.extent(0) - 1;

  for (auto i = offset; i < loc.extent(1) - offset; ++i) {
    const struct Neighbourhood neighbourhood = {loc.at(rowIndex - 1, i),
                                                rowBorders[0].items.at(i),
                                                haloLeft,
                                                haloRight,
                                                loc.at(rowIndex, i)};

    doIteration(locNew, rowIndex, i, neighbourhood);
  }

  // first row
  rowIndex = 0;

  for (auto i = offset; i < loc.extent(1) - offset; ++i) {
    const struct Neighbourhood neighbourhood = {rowBorders[1].items.at(i),
                                                loc.at(rowIndex + 1, i),
                                                haloLeft,
                                                haloRight,
                                                loc.at(rowIndex, i)};

    doIteration(locNew, rowIndex, i, neighbourhood);
  }
}

void calcHaloColumns(const Matrix* m,
                     LocalMatrix& loc,
                     LocalMatrix& locNew,
                     const std::array<HaloBorder, 2>& rowBorders,
                     const std::array<HaloBorder, 2>& columnBorders) {
  constexpr index_t offset = 1;  // # cells already computed by the halo rows
  const Coordinates globalCoordinates = m->pattern().global({0, 0});
  const index_t globalRow = globalCoordinates[0];

  // last column -> using halo for the right
  index_t columnIndex = loc.extent(1) - 1;

  for (auto i = offset; i < loc.extent(0) - offset; ++i) {
    struct Neighbourhood neighbourhood = getNeighbourhood(i, columnIndex, loc);
    neighbourhood.right = columnBorders[0].items.at(globalRow + i);

    doIteration(locNew, i, columnIndex, neighbourhood);
  }

  // first column -> using halo for the left
  columnIndex = 0;

  for (auto i = offset; i < loc.extent(0) - offset; ++i) {
    struct Neighbourhood neighbourhood = getNeighbourhood(i, columnIndex, loc);
    neighbourhood.left = columnBorders[1].items.at(globalRow + i);

    doIteration(locNew, i, columnIndex, neighbourhood);
  }
}

void getHaloRows(const Matrix* theta, std::array<HaloBorder, 2>& halo) {
  for (auto i = 0; i < 2; ++i) {
    getHaloRow(theta, halo[i]);
  }
}

void getHaloColumns(const Matrix* theta, std::array<HaloBorder, 2>& halo) {
  for (auto i = 0; i < 2; ++i) {
    getHaloColumn(theta, halo[i]);
  }
}

void simulate(Matrix* theta, Matrix* thetaNew, index_t nSteps) {
  minimon.enter();

  const index_t numberOfRows = theta->extent(0);
  const index_t numberOfLocalRows = theta->local.extent(0);

  const index_t numberOfColumns = theta->extent(1);
  const index_t numberOfLocalColumns = theta->local.extent(1);

  std::array<HaloBorder, 2> haloRows = {
      {{

           std::vector<val_t>(numberOfColumns),
           dash::Future<val_t*>(),
           haloRowIndex(theta, numberOfLocalRows - 1)

       },
       {std::vector<val_t>(numberOfColumns),
        dash::Future<val_t*>(),
        haloRowIndex(theta, 0)

       }}};

  std::array<HaloBorder, 2> haloColumns = {
      {{std::vector<val_t>(numberOfRows),
        dash::Future<val_t*>(),
        haloColumnIndex(theta, numberOfLocalColumns - 1)},
       {std::vector<val_t>(numberOfRows),
        dash::Future<val_t*>(),
        haloColumnIndex(theta, 0)}}};

  for (auto step = 0; step < nSteps; ++step) {
    minimon.enter();
    {
      LocalMatrix& loc = theta->local;
      LocalMatrix& locNew = thetaNew->local;

      minimon.enter();
      {
        getHaloRows(theta, haloRows);
        getHaloColumns(theta, haloColumns);

        minimon.leave("update halo");
      }

      minimon.enter();
      {
        calcInnerGrid(loc, locNew);

        minimon.leave("calc inner");
      }

      minimon.enter();
      {
        for (auto i = 0; i < 2; ++i) {
          haloRows[i].future.wait();
          haloColumns[i].future.wait();
        }

        minimon.leave("wait halo");
      }

      minimon.enter();
      {
        calcHaloRows(theta, loc, locNew, haloRows, haloColumns);
        calcHaloColumns(theta, loc, locNew, haloRows, haloColumns);

        minimon.leave("calc halo");
      }

      // swap matrices
      std::swap(theta, thetaNew);
      thetaNew->barrier();  // wait for swap
      minimon.leave("calc iter");
    }

#ifdef DEBUGGING
    if (amIMaster()) {
      printf("    ITERATION END %d\n", step);
      printIt(theta);
    }
#endif
  }

  minimon.leave("calc total");
}

void summaryStart(const Matrix* m) {
  if (amIMaster()) {
    printf("Running Jacobi on %d units.\n", dash::Team::All().size());
    printf("Array Dimensions: %d %d\n", m->extent(0), m->extent(1));

    const LocalMatrix& loc = m->local;  // local matrix
    printf("Block Dimensions: %d %d\n", loc.extent(0), loc.extent(1));
  }
}

void summaryEnd(const double eStart, const double eEnd, index_t nSteps) {
  if (amIMaster()) {
    double dE = eEnd - eStart;

    printf("== Energy Conservation Check ==\n");
    printf("initial Energy: %20.4f\n", eStart);
    printf("  final Energy: %20.4f\n", eEnd);
    printf("    Difference: %20.4f\n", dE);
    printf("== Time Statistics ==\n");
    printf("Completed %d iterations.\n", nSteps);
  }
}

void doSimulation(index_t xSize, index_t ySize, index_t nSteps) {
  // setup distributed matrix
  dash::TeamSpec<2> teamspec{};
  auto distspec = dash::DistributionSpec<2>(dash::BLOCKED, dash::NONE);
  Matrix theta(
      dash::SizeSpec<2>(xSize, ySize), distspec, dash::Team::All(), teamspec);
  Matrix thetaNew(
      dash::SizeSpec<2>(xSize, ySize), distspec, dash::Team::All(), teamspec);

  // all zeros
  allocate(&theta);
  allocate(&thetaNew);

  // set starting heat values
  init(&theta);
  init(&thetaNew);
  const double eStart = distributedSum(&theta);
  summaryStart(&theta);

  // perform simulation
  simulate(&theta, &thetaNew, nSteps);

  // end
  const double eEnd = distributedSum(&thetaNew);
  summaryEnd(eStart, eEnd, nSteps);
}

int main(int argc, char* argv[]) {
  dash::init(&argc, &argv);

  minimon.enter();
  {
    std::vector<int> args{};
    int errCode = getArgs(argc, argv, &args);
    if (errCode != 0) {
      return errCode;  // exit with error code
    }

    index_t xSize = args[0], ySize = args[1], nSteps = args[2];
    doSimulation(xSize, ySize, nSteps);
    minimon.leave("total");
  }

  if (amIMaster()) {
    printMinimonSummary(minimon);
  }

  dash::finalize();

  return 0;
}
