#ifndef MINIMONITORING_H
#define MINIMONITORING_H

#include <chrono>
#include <fstream>
#include <iostream>
#include <map>
#include <stack>
#include <vector>

using time_point_t =
    std::chrono::time_point<std::chrono::high_resolution_clock>;
using time_diff_t = std::chrono::duration<double>;

struct MiniMonValue {
  time_diff_t runtime_sum;
  time_diff_t runtime_min;
  time_diff_t runtime_max;
  unsigned int num;

  MiniMonValue()
    : runtime_sum(0.0),
      runtime_min(std::numeric_limits<double>::max()),
      runtime_max(0.0),
      num(0) {}

  void apply(time_diff_t value) {
    runtime_sum += value;
    runtime_min = std::min(runtime_min, value);
    runtime_max = std::max(runtime_max, value);
    num += 1;
  }

  double getAvg() { return runtime_sum.count() / num; }
  double getMin() { return runtime_min.count(); }
  double getMax() { return runtime_max.count(); }
};

class MiniMon {
public:
  MiniMon() = default;

  void enter() { _entries.push(std::chrono::high_resolution_clock::now()); }

  void leave(const char* n) {
    auto& top = _entries.top();
    _store[n].apply(std::chrono::high_resolution_clock::now() - top);
    _entries.pop();
  }

  void print(int id) {
    printf("%2d              name        num      avg      min      max        \%\n", id);
    double longest = 0.0;
    for (auto& e : _store) {
      if (e.second.num > 1) {  // it's repetitive action
        const double avg = e.second.getAvg();
        if (avg > longest) {
          longest = avg;
        }
      }
    }

    for (auto& e : _store) {
      const double avg = e.second.getAvg();
      const double min = e.second.getMin();
      const double max = e.second.getMax();
      const double perc = avg / longest * 100;

      if (e.second.num > 1 && perc < 100) {
        printf("%20s %10d %8.6f %8.6f %8.6f %6.2f \%\n",
               e.first,
               e.second.num,
               avg,
               min,
               max,
               perc);
      } else {
        printf("%20s %10d %8.6f %8.6f %8.6f\n",
               e.first,
               e.second.num,
               avg,
               min,
               max);
      }
    }
  }

private:
  std::map<const char*, MiniMonValue> _store;
  std::stack<time_point_t, std::vector<time_point_t>> _entries;
};

#endif /* MINIMONITORING_H */
