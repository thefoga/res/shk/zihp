#ifndef ZIHP_MATRIX_CHESSBOARD_H
#define ZIHP_MATRIX_CHESSBOARD_H

#include "utils.h"

struct Interval {
    int start;
    int end;
};


/**
 * Divides resources between @nRanks processors
 * @param nRanks number of processors
 * @param nRes number of resources to divide
 * @return intervals
 */
void getDivisions(const int nRanks, const int nRes, struct Interval *intervals) {
    int currentEnd = -1;
    int ranksLeft = nRanks;
    int resLeft = nRes;

    for (int i = 0; i < nRanks; ++i) {
        const int resUsedNow = resLeft / ranksLeft;
        intervals[i].start = currentEnd + 1;

        currentEnd += resUsedNow;
        intervals[i].end = currentEnd + 1;

        ranksLeft -= 1;
        resLeft -= resUsedNow;
    }
}

/**
 * Divides resources between @nRanks processors
 * @param n starts at 0, ends at @nRanks -1
 * @param nRanks number of processors
 * @param nRes number of resources to divide
 * @return interval for the @n-th processor
 */
struct Interval getNDivision(const int n, const int nRanks, const int nRes) {
    struct Interval intervals[6];
    getDivisions(nRanks, nRes, intervals);  // todo faster
    return intervals[n];
}

struct MatrixCoordinates n2Coordinates(const int n, const int rows, const int columns) {
    struct MatrixCoordinates point = {
            n / columns,
            n % columns
    };
    return point;
}

void getBoundaries(const int rank, const int dRows, const int dColumns, const int nRows, const int nColumns,
                   struct MatrixCoordinates *boundaries) {
    struct MatrixCoordinates dRank = n2Coordinates(rank, dRows, dColumns);
    struct Interval rowInterval = getNDivision(dRank.row, dRows, nRows);
    struct Interval columnInterval = getNDivision(dRank.column, dColumns, nColumns);

    boundaries[0].row = rowInterval.start;
    boundaries[0].column = columnInterval.start;

    boundaries[1].row = rowInterval.end;
    boundaries[1].column = columnInterval.end;
}

#endif //ZIHP_PP_H
