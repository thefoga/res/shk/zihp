#ifndef ZIHP_MATRIX_UTILS_H
#define ZIHP_MATRIX_UTILS_H

#include <stdio.h>
#include <mpi.h>

struct MatrixCoordinates {
    int row;
    int column;
};

struct Matrix {
    int rows;
    int columns;
    double *i;  // items
};

int matrix2pointer(const struct MatrixCoordinates coordinates, const int nColumns) {
    return coordinates.row * nColumns + coordinates.column;
}

double getItem(const struct MatrixCoordinates coordinates, const struct Matrix m) {
    int i = matrix2pointer(coordinates, m.columns);
    return m.i[i];
}

void getRow(int row, const struct Matrix m, double *newRow) {
    for (int column = 0; column < m.columns; ++column) {
        struct MatrixCoordinates coordinates = {row, column};
        newRow[column] = getItem(coordinates, m);
    }
}

void getColumn(const struct Matrix m, const int column, double *newColumn) {
    for (int row = 0; row < m.rows; ++row) {
        struct MatrixCoordinates coordinates = {row, column};
        newColumn[row] = getItem(coordinates, m);
    }
}

void printIt(const struct Matrix m) {
    for (int i = 0; i < m.rows; ++i) {
        for (int j = 0; j < m.columns; ++j) {
            struct MatrixCoordinates coordinates = {i, j};
            double item = m.i[matrix2pointer(coordinates, m.columns)];
            printf("%5.2f ", item);
        }
        printf("\n");  // new row -> new line
    }
}

#endif //ZIHP_PP_H
