#ifndef ZIHP_UTILS_H
#define ZIHP_UTILS_H

#include <stdio.h>
#include <mpi.h>

int getRank() {
    int rank;
    MPI_Comm_rank(comm, &rank);  // store rank of MPI process inside communicator
    return rank;
}

int getSize() {
    int size;
    MPI_Comm_size(comm, &size);  // store size MPI processes inside communicator
    return size;
}

void waitForOthers() {
    MPI_Barrier(comm);
}

int askForInt(const char *question) {
    int n;

    printf("%s", question);
    printf("\n");
    scanf("%d", &n);

    return n;
}

#endif //ZIHP_PP_H
