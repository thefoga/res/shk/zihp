#ifndef ZIHP_PP_H
#define ZIHP_PP_H

#include <mpi.h>

#include "utils.h"

void sendPing(double *buffer, const int otherRank, const int msgSize, const int ping, const int pong) {
    MPI_Send(buffer, msgSize, MPI_DOUBLE, otherRank, ping, MPI_COMM_WORLD);  // send a ping
    MPI_Recv(buffer, msgSize, MPI_DOUBLE, otherRank, pong, MPI_COMM_WORLD,
             MPI_STATUS_IGNORE);  // ... and wait for a pong
#ifdef DEBUG
    int rank = getRank();
    printf("%d: PING\n", rank);
#endif
}

void doPing(const int otherRank, const int nMessages, const int msgSize, const int ping, const int pong) {
    double buffer[msgSize];

    for (int i = 0; i < nMessages; ++i) {
        sendPing(buffer, otherRank, msgSize, ping, pong);
    }
}

void sendPong(double *buffer, const int otherRank, const int msgSize, const int ping, const int pong) {
    MPI_Recv(buffer, msgSize, MPI_DOUBLE, otherRank, ping, MPI_COMM_WORLD, MPI_STATUS_IGNORE);  // wait for a ping
    MPI_Send(buffer, msgSize, MPI_DOUBLE, otherRank, pong, MPI_COMM_WORLD);  // ... and send a pong
#ifdef DEBUG
    int rank = getRank();
    printf("%d: PONG\n", rank);
#endif
}

void doPong(const int otherRank, const int nMessages, const int msgSize, const int ping, const int pong) {
    double buffer[msgSize];

    for (int i = 0; i < nMessages; ++i) {
        sendPong(buffer, otherRank, msgSize, ping, pong);
    }
}

#endif //ZIHP_PP_H
