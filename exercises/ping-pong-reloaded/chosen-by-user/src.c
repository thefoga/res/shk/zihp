#include <stdio.h>
#include <mpi.h>

#define DEBUG
#define comm MPI_COMM_WORLD

#include "../../libs/utils.h"
#include "../../libs/pp.h"

int main(int argc, char *argv[]) {
    MPI_Init(&argc, &argv);

    const int rank = getRank();
    const int nMessages = 50;
    const int msgSize = 8 * 64 << 2;

    const int rankInfo = 42;
    const int ping = 17;
    const int pong = 23;

    int buffer[1];

    if (rank == 0) {
        int otherRank = askForInt("Enter rank of other ping-pong player");
        buffer[0] = otherRank;

        for (int i = 1; i < getSize(); ++i) {
            MPI_Send(buffer, 1, MPI_INT, i, rankInfo, MPI_COMM_WORLD);  // send rank info
        }

        doPing(otherRank, nMessages, msgSize, ping, pong);
    } else {
        MPI_Recv(buffer, 1, MPI_INT, 0, rankInfo, MPI_COMM_WORLD, MPI_STATUS_IGNORE);  // wait for rank info
        if (buffer[0] == rank) {  // I am the right rank
            const int otherRank = 0;
            doPong(otherRank, nMessages, msgSize, ping, pong);
        }
    }

    MPI_Finalize();
    return 0;
}