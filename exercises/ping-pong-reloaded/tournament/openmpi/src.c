#include <stdio.h>
#include <mpi.h>

#define comm MPI_COMM_WORLD
#define NON_BLOCKING

#include "../../../libs/utils.h"
#include "../../../libs/pp.h"


void tellOthersMyOpponent(const int opponent, const int tag) {
#ifdef NON_BLOCKING
    MPI_Request send_handle;
#endif

    int buffer[1] = {opponent};

    for (int i = 0; i < getSize(); ++i) {
        if (i != getRank()) {
#ifdef NON_BLOCKING
            MPI_Isend(buffer, 1, MPI_INT, i, tag, comm, &send_handle);  // send rank info
#else
            MPI_Send(buffer, 1, MPI_INT, i, tag, MPI_COMM_WORLD);  // send rank info
#endif
        }
    }
}

void everybodyPlay(const int nMessages, const int msgSize, const int secondPlayerInfo,
                   const int ping, const int pong) {
#ifdef NON_BLOCKING
    MPI_Request recv_handle;
#endif

    for (int firstPlayer = 0; firstPlayer < getSize(); ++firstPlayer) {
        if (firstPlayer != getRank()) {  // I am not the first player. Listen then maybe play
            for (int i = 0; i < getSize() - 1; ++i) {  // receiving n - 1 plays
                int secondPlayerBuffer[1];
#ifdef NON_BLOCKING
                MPI_Irecv(secondPlayerBuffer, 1, MPI_INT, firstPlayer, secondPlayerInfo, comm, &recv_handle);
#else
                MPI_Recv(secondPlayerBuffer, 1, MPI_INT, firstPlayer, secondPlayerInfo, MPI_COMM_WORLD,
                         MPI_STATUS_IGNORE);
#endif
                int secondPlayer = secondPlayerBuffer[0];
                if (secondPlayer == getRank()) {
                    doPong(firstPlayer, nMessages, msgSize, ping, pong);
                }
            }
        } else {  // I am the first player! Tell others, then play
            for (int other = 0; other < getSize(); ++other) {
                if (other != getRank()) {
                    tellOthersMyOpponent(other, secondPlayerInfo);

                    double timer = MPI_Wtime();
                    doPing(other, nMessages, msgSize, ping, pong);
                    timer = MPI_Wtime() - timer;
                    const double playTime = timer * 1e6;

                    printf("%d,%d,%f\n", firstPlayer, other, playTime);
                }
            }
        }
    }
}

int main(int argc, char *argv[]) {
    MPI_Init(&argc, &argv);

    const int nMessages = 100;
    const int msgSize = 8 * 64 << 10;

    const int ping = 17;
    const int pong = 23;

    const int secondPlayerInfo = 23;

    const char *dataHeader = "first,second,play time";
    if (getRank() == 0) {
        printf("%s\n", dataHeader);
    }

    everybodyPlay(nMessages, msgSize, secondPlayerInfo, ping, pong);

    MPI_Finalize();
    return 0;
}
