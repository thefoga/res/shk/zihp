#include <libdash.h>
#include <chrono>

using namespace std::chrono;

double getUnixMs() {
    const auto p0 = std::chrono::time_point < std::chrono::system_clock > {};
    std::time_t epoch_time = std::chrono::system_clock::to_time_t(p0);
    return epoch_time;
}

void everybodyPlay(const int nMessages, const int msgSize) {
    // create distributed matrix
    dash::TeamSpec<2> teamspec{};
    auto distspec = dash::DistributionSpec<2>(dash::BLOCKED, dash::NONE);  // 1 row for each rank  // todo how to chessboard ???
    // contains 1 meassage for each row, 1 row for each rank => 1 message for each rank
    dash::NArray<double, 2> mat(
            dash::SizeSpec<2>(dash::size(), msgSize), distspec, dash::Team::All(), teamspec);

    // fill the distributed matrix
    for (int row = 0; row < mat.extent(0); ++row) {
        for (int col = 0; col < mat.extent(1); ++col) {
            mat.at(row, col) = 0.0;
        }
    }
    mat.barrier();  // wait until all have filled it

    for (int firstPlayer = 0; firstPlayer < dash::size(); ++firstPlayer) {
        for (int secondPlayer = 0; secondPlayer < dash::size(); ++secondPlayer) {
            if (firstPlayer != secondPlayer) {
                double timer = getUnixMs();

                // play ping pong
                for (int i = 0; i < nMessages; ++i) {
                    // send-like
                    for (int col = 0; col < mat.extent(1); ++col) {
                        mat.at(secondPlayer, col) = mat.at(firstPlayer, col);
                    }

                    // receive-like
                    for (int col = 0; col < mat.extent(1); ++col) {
                        mat.at(firstPlayer, col) = mat.at(secondPlayer, col);
                    }
                }

                timer = getUnixMs() - timer;
                printf("%d,%d,%f\n", firstPlayer, secondPlayer, timer);
            }
        }
    }
}

int main(int argc, char *argv[]) {
    dash::init(&argc, &argv);

    const int nMessages = 100;
    const int msgSize = 8 * 64 << 2;

    const char *dataHeader = "first,second,play time";
    if (dash::myid() == 0) {
        printf("%s\n", dataHeader);
    }

    everybodyPlay(nMessages, msgSize);

    dash::finalize();
    return 0;
}
