#!/usr/bin/env python
# coding: utf-8

import sys
from io import StringIO

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


def parse_input(inp):
    """
    :return: pd.DataFrame with tournament data
    """

    inp = StringIO(inp)
    data = pd.read_csv(inp)

    data['first'] = data['first'].astype(int)
    data['second'] = data['second'].astype(int)
    data['play time'] = data['play time'].astype(float)

    return data


def get_matrix(data):
    n = int((1 + np.sqrt(len(data) * 4 + 1)) / 2)  # solve n(n-1) = x
    matrix = np.zeros((n, n))
    for row in range(n):
        for col in range(n):
            if row != col:
                play_time = data[data['first'] == row][data['second'] == col]['play time']
                matrix[row][col] = play_time

            matrix[row][row] = float('inf')

    return matrix


def show_map(matrix, threshold=0.9):
    max_value = np.quantile(matrix, threshold)
    plt.imshow(matrix, cmap='jet', origin='lower', vmax=max_value)

    plt.xticks(range(matrix.shape[1]))  # add ticks
    plt.yticks(range(matrix.shape[0]))

    plt.colorbar(extend='max')  # add label
    plt.savefig('heatmap.png', papertype='a2', dpi=1000)  # save to file


def main():
    stdin = sys.stdin.read()
    data = parse_input(stdin)
    matrix = get_matrix(data)
    show_map(matrix)


if __name__ == '__main__':
    main()
