#include <stdio.h>
#include <mpi.h>


void reduce(int my_rank) {
    int sum = 0;
    int snd_buf[1] = {my_rank};
    int rcv_buf[1] = {0};

    MPI_Scan(snd_buf, rcv_buf, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
    sum = rcv_buf[0];
    printf("rank= %i -> sum=%i\n", my_rank, sum);
}

int main(int argc, char *argv[]) {
    MPI_Init(&argc, &argv);

    int my_rank;

    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);  // store rank of MPI process

    reduce(my_rank);

    MPI_Finalize();
    return 0;
}