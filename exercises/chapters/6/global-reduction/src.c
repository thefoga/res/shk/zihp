#include <stdio.h>
#include <mpi.h>


void reduce(int my_rank, int size) {
    int sum = 0;
    int snd_buf[1] = {my_rank};
    int rcv_buf[1] = {0};

    MPI_Allreduce(snd_buf, rcv_buf, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
    sum = rcv_buf[0];
    printf("%i/%i: %i\n", my_rank, size, sum);
}

int main(int argc, char *argv[]) {
    MPI_Init(&argc, &argv);

    int my_rank, size;

    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);  // store rank of MPI process
    MPI_Comm_size(MPI_COMM_WORLD, &size);  // store size of all MPI processes

    reduce(my_rank, size);

    MPI_Finalize();
    return 0;
}