#include <stdio.h>
#include <mpi.h>


int main(int argc, char *argv[]) {
    MPI_Init(&argc, &argv);

    int rank, size;

    int buffer[1];
    buffer[0] = 0;

    MPI_Status status;
    const int tag = 0;

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);  // store rank of MPI process
    MPI_Comm_size(MPI_COMM_WORLD, &size);  // store size of all MPI processes

    if (rank == 0) {
        printf("I am %i of %i\n", rank, size);
        buffer[0] = 1;
        MPI_Send(buffer, 1, MPI_INT, 1, tag, MPI_COMM_WORLD);
    } else {
        int previousRank = rank - 1;
        int nextRank = rank + 1;

        if (previousRank >= 0) {
            MPI_Recv(buffer, 1, MPI_INT, previousRank, tag, MPI_COMM_WORLD, &status);
        }

        if (buffer[0] > 0) {
            printf("I am %i of %i\n", rank, size);

            if (nextRank < size) {
                MPI_Send(buffer, 1, MPI_INT, rank + 1, tag, MPI_COMM_WORLD);
            }
        }
    }

    MPI_Finalize();
    return 0;
}
