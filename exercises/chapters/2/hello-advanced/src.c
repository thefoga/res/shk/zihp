#include <stdio.h>
#include <mpi.h>


int main(int argc, char *argv[]) {
    MPI_Init(&argc, &argv);

    int rank, size;

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);  // store rank of MPI process
    MPI_Comm_size(MPI_COMM_WORLD, &size);  // store size of all MPI processes

    if (rank == 0) {  // main process
        printf("Hello world. MPI version is %i.%i\n", MPI_VERSION, MPI_SUBVERSION);
    } else {
        printf("I am %i of %i\n", rank, size);
    }

    MPI_Finalize();
    return 0;
}
