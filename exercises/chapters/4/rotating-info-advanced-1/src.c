#include <stdio.h>
#include <mpi.h>


void rotate(int my_rank, int size) {
    int sum = 0;
    int snd_buf[1] = {my_rank};
    int rcv_buf[1] = {0};

    int neighbour2TheRight = (my_rank + 1) % size;  // next (send info to)
    int neighbour2TheLeft = (my_rank - 1 + size) % size;  // previous (get info from)

    MPI_Status status[2];
    MPI_Request request[2];

    for (int i = 0; i < size; ++i) {
        MPI_Issend(snd_buf, 1, MPI_INT, neighbour2TheRight, 0, MPI_COMM_WORLD, &request[0]);
        MPI_Irecv(rcv_buf, 1, MPI_INT, neighbour2TheLeft, 0, MPI_COMM_WORLD, &request[1]);
        MPI_Waitall(2, request, status);

        sum += rcv_buf[0];
        snd_buf[0] = rcv_buf[0];
    }

    printf("%i/%i: %i\n", my_rank, size, sum);
}

int main(int argc, char *argv[]) {
    MPI_Init(&argc, &argv);

    int my_rank, size;

    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);  // store rank of MPI process
    MPI_Comm_size(MPI_COMM_WORLD, &size);  // store size of all MPI processes

    rotate(my_rank, size);

    MPI_Finalize();
    return 0;
}