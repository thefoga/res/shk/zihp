#include <stdio.h>
#include <mpi.h>


#define DEBUG

void sendPing(double *buffer, int otherRank, const int msgSize, const int ping, const int pong) {
    MPI_Status status;

    MPI_Send(buffer, msgSize, MPI_DOUBLE, otherRank, ping, MPI_COMM_WORLD);  // send a ping
    MPI_Recv(buffer, msgSize, MPI_DOUBLE, otherRank, pong, MPI_COMM_WORLD, &status);  // ... and wait for a pong
#ifdef DEBUG
    printf("-> PING\n");
#endif
}

int doPing(double *buffer, const int nMessages, const int msgSize, const int ping, const int pong) {
    int otherRank = 1;
    double timer = MPI_Wtime();  // measure loop

    for (int i = 0; i < nMessages; ++i) {
        sendPing(buffer, otherRank, msgSize, ping, pong);
    }

    timer = MPI_Wtime() - timer;
    timer = timer / (2 * nMessages) * 1e6;
    return (int) timer;
}

void sendPong(double *buffer, int otherRank, const int msgSize, const int ping, const int pong) {
    MPI_Status status;

    MPI_Recv(buffer, msgSize, MPI_DOUBLE, otherRank, ping, MPI_COMM_WORLD, &status);  // wait for a ping
    MPI_Send(buffer, msgSize, MPI_DOUBLE, otherRank, pong, MPI_COMM_WORLD);  // ... and send a pong
#ifdef DEBUG
    printf("<- PONG\n");
#endif
}

int doPong(double *buffer, const int nMessages, const int msgSize, const int ping, const int pong) {
    int otherRank = 0;
    for (int i = 0; i < nMessages; ++i) {
        sendPong(buffer, otherRank, msgSize, ping, pong);
    }

    return 0;
}

void pingPong(const int rank, const int nMessages, const int msgSize, const int ping, const int pong) {
    double buffer[msgSize];

    if (rank == 0) {
        int transfer_time = doPing(buffer, nMessages, msgSize, ping, pong);
        printf("\nTransfer time of one message: %i us", transfer_time);
    } else {
        doPong(buffer, nMessages, msgSize, ping, pong);
    }
}

int main(int argc, char *argv[]) {
    MPI_Init(&argc, &argv);

    int rank;
    const int nMessages = 50;
    const int msgSize = 1e4;

    const int ping = 17;
    const int pong = 23;

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);  // store rank of MPI process

    pingPong(rank, nMessages, msgSize, ping, pong);  // rotating-info

    MPI_Finalize();
    return 0;
}