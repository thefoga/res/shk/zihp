#ifndef ZIHP_MATRIX_COMMON_H
#define ZIHP_MATRIX_COMMON_H

double filler(int row, int column) {
    return (row + column) + getRank() / 100.0;
}

void fillMatrix(const struct Matrix m, double startingValue) {
    for (int i = 0; i < m.rows; ++i) {
        for (int j = 0; j < m.columns; ++j) {
            struct MatrixCoordinates coordinates = {i, j};
            m.i[matrix2pointer(coordinates, m.columns)] = startingValue + filler(i, j);
        }
    }
}

#endif //ZIHP_MATRIX_COMMON_H
