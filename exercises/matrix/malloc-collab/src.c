#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

#define comm MPI_COMM_WORLD
#define NON_BLOCKING

#include "../../libs/utils.h"
#include "../../libs/matrix/chessboard.h"
#include "../../libs/matrix/utils.h"
#include "../common.h"


void receiveMatrix(const struct Matrix m, int other, const int tag) {
#ifdef NON_BLOCKING
    MPI_Request recv_handle;
#endif

    int coordinates[4];  // start row, start column, end row, end column
#ifdef NON_BLOCKING
    MPI_Irecv(coordinates, 4, MPI_INT, other, tag, comm, &recv_handle);
    MPI_Wait(&recv_handle, MPI_STATUS_IGNORE);  // wait for it
#else
    MPI_Recv(coordinates, 4, MPI_INT, other, tag, comm, MPI_STATUS_IGNORE);
#endif

    struct MatrixCoordinates start = {coordinates[0], coordinates[1]};
    struct MatrixCoordinates end = {coordinates[2], coordinates[3]};
    int spanColumns = end.column - start.column;

    for (int row = start.row; row < end.row; ++row) {  // alloc per row
        double newRow[spanColumns];
#ifdef NON_BLOCKING
        MPI_Irecv(newRow, spanColumns, MPI_DOUBLE, other, tag, comm, &recv_handle);
        MPI_Wait(&recv_handle, MPI_STATUS_IGNORE);  // wait for it
#else
        MPI_Recv(newRow, spanColumns, MPI_DOUBLE, other, tag, comm, MPI_STATUS_IGNORE);
#endif

        // fill matrix with received row (per column)
        for (int column = start.column; column < end.column; ++column) {
            struct MatrixCoordinates item = {row, column};
            int prevI = column - start.column;
            int newI = matrix2pointer(item, m.columns);
            m.i[newI] = newRow[prevI];
        }
    }
}

void sendMyMatrix(struct MatrixCoordinates boundaries[], const struct Matrix m, int other, const int tag) {
#ifdef NON_BLOCKING
    MPI_Request send_handle;
#endif
    // send info regarding boundaries
    int buffer[4] = {
            boundaries[0].row,
            boundaries[0].column,
            boundaries[1].row,
            boundaries[1].column
    };  // start row, start column, end row, end column

#ifdef NON_BLOCKING
    MPI_Isend(buffer, 4, MPI_INT, other, tag, comm, &send_handle);
#else
    MPI_Send(buffer, 4, MPI_INT, other, tag, comm);
#endif

    // send actual matrix
    for (int row = 0; row < m.rows; ++row) {  // send per row
        double newRow[m.columns];  // alloc new row
        getRow(row, m, newRow);  // get row
#ifdef NON_BLOCKING
        MPI_Isend(newRow, m.columns, MPI_DOUBLE, other, tag, comm, &send_handle);  // send it
#else
        MPI_Send(newRow, m.columns, MPI_DOUBLE, other, tag, comm);  // send it
#endif
    }
}

void doParallel(const int chessboardRows, const int chessboardColumns, const int sideLength, const int tag) {
    struct MatrixCoordinates boundaries[2];
    getBoundaries(getRank(), chessboardRows, chessboardColumns, sideLength, sideLength, boundaries);

    const int nRows = boundaries[1].row - boundaries[0].row;
    const int nColumns = boundaries[1].column - boundaries[0].column;
    double *myMatrix = malloc(nRows * nColumns * sizeof(double));
    const struct Matrix m = {
            nRows,
            nColumns,
            myMatrix
    };

    double startingValue = boundaries[0].row + boundaries[0].column;
    fillMatrix(m, startingValue);
    waitForOthers();

    if (getRank() == 0) {
        double *allMatrix = malloc(sideLength * sideLength * sizeof(double));
        const struct Matrix fullM = {
                sideLength,
                sideLength,
                allMatrix
        };

        for (int row = 0; row < m.rows; ++row) {  // fill my part of matrix
            for (int column = 0; column < m.columns; ++column) {
                struct MatrixCoordinates item = {row, column};
                int prevI = matrix2pointer(item, m.columns);
                int newI = matrix2pointer(item, fullM.columns);
                fullM.i[newI] = m.i[prevI];
            }
        }

        for (int other = 1; other < getSize(); ++other) {  // will get info from this rank
            receiveMatrix(fullM, other, tag);
        }

        printIt(fullM);
        free(allMatrix);
    } else {
        sendMyMatrix(boundaries, m, 0, tag);
    }

    waitForOthers();
    free(myMatrix);
}

int main(int argc, char *argv[]) {
    MPI_Init(&argc, &argv);

    const int sideLength = 17;  // todo user should provide these
    const int chessboardRows = 5;
    const int chessboardColumns = 4;
    const int msgTag = 44;

    doParallel(chessboardRows, chessboardColumns, sideLength, msgTag);

    MPI_Finalize();
    return 0;
}
