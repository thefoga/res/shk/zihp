#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

#define comm MPI_COMM_WORLD

#include "../../libs/utils.h"
#include "../../libs/matrix/chessboard.h"
#include "../../libs/matrix/utils.h"
#include "../common.h"


void doSequential(const int nRows, const int nColumns) {
    if (getRank() == 0) {  // only 0 should do it
        double *matrix = malloc(nRows * nColumns * sizeof(double));
        const struct Matrix m = {
                nRows,
                nColumns,
                matrix
        };
        fillMatrix(m, 0, &filler);
        printIt(m);
        free(matrix);
        waitForOthers();
    } else {
        waitForOthers();
    }
}

void doParallel(const int chessboardRows, const int chessboardColumns, const int sideLength) {
    struct MatrixCoordinates boundaries[2];
    getBoundaries(getRank(), chessboardRows, chessboardColumns, sideLength, sideLength, boundaries);

    const int nRows = boundaries[1].row - boundaries[0].row;
    const int nColumns = boundaries[1].column - boundaries[0].column;
    double *matrix = malloc(nRows * nColumns * sizeof(double));
    const struct Matrix m = {
            nRows,
            nColumns,
            matrix
    };

    double startingValue = boundaries[0].row + boundaries[0].column;
    fillMatrix(m, startingValue);

    for (int printer = 0; printer < getSize(); ++printer) {
        if (printer == getRank()) {
            printIt(m);
            free(matrix);
            waitForOthers();
        } else {
            waitForOthers();
        }
    }
}

int main(int argc, char *argv[]) {
    MPI_Init(&argc, &argv);

    int sideLength = 17;  // todo user should provide these
    int chessboardRows = 5;
    int chessboardColumns = 4;

    doSequential(sideLength, sideLength);
    doParallel(chessboardRows, chessboardColumns, sideLength);

    MPI_Finalize();
    return 0;
}
