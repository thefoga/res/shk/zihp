#include <libdash.h>

void doParallel(const int sideLength) {
    // create distributed matrix
    dash::TeamSpec<2> teamspec{};
    auto distspec = dash::DistributionSpec<2>(dash::BLOCKED, dash::NONE);  // todo how to chessboard ???
    dash::NArray<double, 2> mat(
            dash::SizeSpec<2>(sideLength, sideLength), distspec, dash::Team::All(), teamspec);

    // fill the distributed matrix
    auto loc = mat.local;   // this unit's local view on the matrix
    const int nRows = loc.extent(0);
    const int nCols = loc.extent(1);
    for (int i = 0; i < nRows; ++i) {
        for (int j = 0; j < nCols; ++j) {
            const long int localIndex = i * nCols + j;  // todo why not /opt/dash/include/dash/Pattern.h:334
            // long int globalIndex = mat.pattern().global(localIndex);
            auto globalIndex = mat.pattern().global({i, j});

            const long int globalRow = globalIndex.at(0); // / sideLength;
            const long int globalCol = globalIndex.at(1); // % sideLength;
            loc.at(i, j) = globalRow + globalCol + dash::myid() / 100.0;
        }
    }
    mat.barrier();  // wait until all have filled it

    if (dash::myid() == 0) {
        for (int i = 0; i < mat.extent(0); ++i) {
            for (int j = 0; j < mat.extent(1); ++j) {
                double item = mat(i, j);
                printf("%5.2f ", item);
            }
            printf("\n");  // new row -> new line
        }
    }
}

int main(int argc, char *argv[]) {
    dash::init(&argc, &argv);

    const int sideLength = 17;
    doParallel(sideLength);

    dash::finalize();
    return 0;
}
